﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaBatailleDuCafe
{   
    //Classe instanciant les méthodes de la carte 
    public class Map
    {

        private List<Case> mapList; //Déclaration d'une liste contenant les cases
        private Case[,] mapTab; //Déclaration d'un tableau de case 
        
        //Constructeur de la carte
        public Map()
        {
            this.mapList = new List<Case>();
            this.mapTab = new Case[10, 10];
        }

        //Procédure qui instancie les types des cases et les frontières en passant en paramètre la trame récupéré précédemment
        //On parcours la trame grâce aux "|" et aux ":"
        public void StorageMapList(string trame)
        {
            foreach (string ligne in trame.Split('|'))
            {
                foreach (string nbr in ligne.Split(':'))
                {
                    Case caseIsland = new Case(Convert.ToInt32(nbr));
                    caseIsland.WhatNature();
                    caseIsland.WhatFrontier();
                    this.GetMapList().Add(caseIsland);

                }

            }
        }

        //Procédure de stockage des cases dans la mapTab a pour paramètre la liste des cases
        public void StorageMapTab(List<Case> listMap)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    this.GetMapTab()[i, j] = listMap.ElementAt(0); //stockage de l'élément de la liste à la ligne 0
                    listMap.RemoveAt(0); //suppression de l'élément à la ligne 0

                }
            }

        }

        //Gette récupérant la liste des cases 
        public List<Case> GetMapList()
        {
            return this.mapList;
        }
        //Gette récupérant le tableau de cases
        public Case[,] GetMapTab()
        {
            return this.mapTab;
        }
        //Procédure affichant la carte en mode console grâce à 2 boucles imbriquées
        public void ShowTheMap()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    //Affichage de l'identifiant correspondant à la case 
                    Console.Write(this.GetMapTab()[i, j].GetIdentifer() + " ");
                }
                Console.WriteLine();
            }
            
        }
        //Procédure associant des identifiants aux cases de la carte 
        public void InitialisationIdentifier()
        {

            char identifer = 'a'; //On initialise le premier identifiant à la lettre "a"
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    //On vérifie si la case est de type "Terre" et si l'identifiant de celle ci n'est pas déjà attribué et place l'identifiant  
                    if ((this.GetMapTab()[i, j].IsFields()) && (this.GetMapTab()[i, j].GetIdentifer() == ' '))
                    {
                        this.GetMapTab()[i, j].SetIdentifier(identifer);
                        this.AttributionParcelle(i, j, identifer);
                        identifer++;
                    }
                }
            }

        }

        //Procédure d’attribution des parcelles prenant en paramètre: la ligne, la colonne et l’identifiant de la case 
        public void AttributionParcelle(int rows, int column, char identifier)
        {
            //Vérifie si la case courante à une frontière au nord 
            if (this.GetMapTab()[rows, column].HasFrontierNorth() == false)
            {
                //Si oui on vérifie si la case au nord de la case courant n'a pas d'identifiant 
                if (this.GetMapTab()[rows - 1, column].GetIdentifer() == ' ')
                {
                    //Positionne l'identifiant de la case courante correspondant à l'identifiant de la case au nord
                    this.GetMapTab()[rows - 1, column].SetIdentifier(identifier);
                    this.AttributionParcelle(rows - 1, column, identifier);
                }
            }
            //Même chose que précédemment mais pour les autres frontières
            if (this.GetMapTab()[rows, column].HasFrontierEast() == false)
            {
                if (this.GetMapTab()[rows, column + 1].GetIdentifer() == ' ')
                {
                    this.GetMapTab()[rows, column + 1].SetIdentifier(identifier);
                    this.AttributionParcelle(rows, column + 1, identifier);
                }
            }
            if (this.GetMapTab()[rows, column].HasFrontierSouth() == false)
            {
                if (this.GetMapTab()[rows + 1, column].GetIdentifer() == ' ')
                {
                    this.GetMapTab()[rows + 1, column].SetIdentifier(identifier);
                    this.AttributionParcelle(rows + 1, column, identifier);
                }
            }
            if (this.GetMapTab()[rows, column].HasFrontierWest() == false)
            {
                if (this.GetMapTab()[rows, column - 1].GetIdentifer() == ' ')
                {
                    this.GetMapTab()[rows, column - 1].SetIdentifier(identifier);
                    this.AttributionParcelle(rows, column - 1, identifier);
                }
            }
        }

        //Procédure calculant la taille des parcelles
        public void CalculTailleParcelle()
        {
            //Instanciation des variables nécessaires au comptage
            int codeAsciiDebut = 97; // code ascii de "a"
            int codeAsciiCourant;
            
            for (int nbLettre = 0; nbLettre < 17; nbLettre++)
            {
                int nbCaseParcelle = 0;

                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        codeAsciiCourant = mapTab[i, j].GetIdentifer();
                        // on met l'identifiant de la case dans un int, cela devient le code ascii du char
                        if (codeAsciiCourant == codeAsciiDebut)
                        {
                            nbCaseParcelle++;
                        }
                    }
                }
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {

                        codeAsciiCourant = mapTab[i, j].GetIdentifer();
                        // on met l'identifiant de la case dans un int, cela devient le code ascii du char
                        if (codeAsciiCourant == codeAsciiDebut)
                        {
                            mapTab[i, j].SetTailleParcelle(nbCaseParcelle);
                        }
                    }
                }

                codeAsciiDebut++;
            }
        }

    }
}
