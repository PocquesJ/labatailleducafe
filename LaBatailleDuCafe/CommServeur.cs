﻿using System.Net;
using System.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaBatailleDuCafe
{
    //Classe permettant la récupération de la trame, l'envoie et la reception entre le serveur et notre IA
    class CommServeur
    {
        Encoding ASCII = Encoding.ASCII;
        byte[] envoie;
        byte[] reception;
        string messagerecu = null;
        private static Socket SocketComm;

        public Socket getSocketComm()
        {
            return SocketComm;
        }

        //Procédure qui va créer la connection avec le serveur, prend en paramètre l'adresse en chaîne de caractère et le port en nombre entier
        public static void Connection(string adresse, int port)
        {
            SocketComm = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            SocketComm.Connect(adresse, port);

        }

        //Procédure qui gère la réception des chaînes de caractère envoyé par le serveur, prend en paramètre la longueur de la chaîne de caractère et retourne la chaîne reçu
        public static String Reception(int longueur)
        {
            byte[] reception = new byte[longueur];
            SocketComm.Receive(reception); //Utilisation de la méthode "Receive" des sockets
            String messageRecu = Encoding.ASCII.GetString(reception);
            return messageRecu;
        }

        //Procédure qui gère l'envoi des coordonnées jouées par notre IA, prend en paramètre une chaîne de caractère (A:xy)
        public static void Envoi(string CoordAEnvoyer)
        {
            byte[] envoie = Encoding.ASCII.GetBytes(CoordAEnvoyer);
            SocketComm.Send(envoie); //Utilisation de la méthode "Send" des sockets
        }

        //Procédure qui gère la déconnection au serveur
        public static void Deco(Socket SocketComm)
        {
            SocketComm.Shutdown(SocketShutdown.Both); //On utilise les méthodes des sockets "shutdown" et "close" pour mettre fin à la connection
            SocketComm.Close();
        }


    }
}
