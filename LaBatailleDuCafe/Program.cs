﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace LaBatailleDuCafe
{
    class Program
    {
        //Programme principale 
        static void Main(string[] args)
        {
            //Déclaration d'une nouvelle carte
            Map unemap = new Map();
            CommServeur.Connection("localhost", 1213);  //Connection au serveur via localhost et le port 1213
            //Décodage et initalisation de la carte dans son intégralité
            unemap.StorageMapList(CommServeur.Reception(500).TrimEnd('\0').TrimEnd('|')); 
            unemap.StorageMapTab(unemap.GetMapList());
            unemap.InitialisationIdentifier();
            unemap.CalculTailleParcelle();
            unemap.ShowTheMap();
            //Création de l'instance de l'IA
            IA etreSupérieur = new IA(unemap);
            //Lancement de l'IA
            etreSupérieur.Cerveau();
            Console.ReadKey();


        }
    }
}
