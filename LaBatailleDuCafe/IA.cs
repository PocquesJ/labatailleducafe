﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaBatailleDuCafe
{
    //Instanciation de la classe IA
    class IA
    {
        //On instancie la carte du jeu 
        private Map mapDeJeu;

        public IA(Map uneAutreMap)
        {
            this.mapDeJeu = uneAutreMap;
        }

        //Fonction qui retourne les poses possibles par l'IA, prend en paramètre l'abscisse et l'ordonnée ou le serveur a posé en dernier et la taille de la parcelle recherché
        public List<String> PosesPossible(int x, int y, int tailleVoulu)
        {
            //On instancie une nouvelle liste des potentiels placements par l'IA
            List<String> potentielPose = new List<string>();

            //On récupère l'identifier de la dernière pose du serveur car le placement sur la même parcelle du dernier coup joué est interdit
            char identifierParcelle = mapDeJeu.GetMapTab()[x, y].GetIdentifer();

            //On parcours la ligne ou la dernière graîne a été posé
            for (int i = 0; i < 10; i++)
            {
                //Si la case et de la taille voulu, n'a pas le même identifiant que la dernière pose et qu'elle n'est à personne on ajoute cette case à notre liste
                if (mapDeJeu.GetMapTab()[x, i].GetTailleParcelle() == tailleVoulu && mapDeJeu.GetMapTab()[x, i].GetCestChezQui() == "personne" && identifierParcelle != mapDeJeu.GetMapTab()[x, i].GetIdentifer())
                {
                    potentielPose.Add("" + x + i);
                }

            }
            for (int i2 = 0; i2 < 10; i2++)
            {
                //Pareil qu'au dessus mais pour les colonnes
                if (mapDeJeu.GetMapTab()[i2, y].GetTailleParcelle() == tailleVoulu && mapDeJeu.GetMapTab()[i2, y].GetCestChezQui() == "personne" && identifierParcelle != mapDeJeu.GetMapTab()[i2,y].GetIdentifer())
                {
                    potentielPose.Add("" + i2 + y);
                }

            }
            return potentielPose; //On retourne la liste des poses possibles
        }

        //Fonction booléenne prennant en paramètre les coordonnées et le nombre nécessaire à la conquête d'une parcelle (2 pour une parcelle de 3, 3 pour une parcelle de 4 et 4 pour une parcelle de 6)
        public bool OnPeuxJouerOuPas(string coord,int majorite)
        {
            //On récupère l'abscisse et l'ordonnée de la dernière pose
            int x = int.Parse(coord.Substring(0, 1));
            int y = int.Parse(coord.Substring(1, 1));
            int coupJoueDansParcelleParServeur = 0; //On instancie un compteur pour compter le nombre de graîne que possède le serveur sur la parcelle
            char identifierParcelle = mapDeJeu.GetMapTab()[x, y].GetIdentifer();

            //On parcoure toute la carte
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    //Si l'identifiant est le même que l'identifiant de la parcelle recherché, et que cette case appartient au serveur, on ajoute 1 à notre compteur
                    if (mapDeJeu.GetMapTab()[i, j].GetIdentifer() == identifierParcelle && mapDeJeu.GetMapTab()[i, j].GetCestChezQui() == "serveur")
                    {
                        coupJoueDansParcelleParServeur++; 
                    }
                }

            }

            //Si le serveur à joué assez de coup dans cette parcelle on retourne false
            if (coupJoueDansParcelleParServeur >= majorite)
            {
                return false;
            }
            //Sinon on retourne true et on peut jouer sur cette parcelle sans gaspiller un coup
            else
            {
                return true;
            }
        }
                
        //Fonction de la stratégie de secours
        //Si aucune case ne peut être joué stratégiquement, afin de ne pas gaspiller une graîne on parcourt toute la carte et place une graîne dés que possible
        //Prend en paramètre les coordonnées de la dernière pose de graîne
        public string StratSecours(int x, int y)
        {
            string coordsec = null;
            bool fini = false;

            //On parcourt la ligne et la colonne de la dernière pose
            while (!fini)
            {
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        //Si il y a une case vide, de type de "Terre", on termine la boucle et on passe le booléen à true pour sortir du "tant que"
                        if (mapDeJeu.GetMapTab()[i, y].IsFields() && (mapDeJeu.GetMapTab()[i, y].GetCestChezQui() == "personne"))
                        {
                            coordsec = i.ToString() + y.ToString(); //On concaténe les coordonnées pour les retourner à la fin dans une chaîne de caractère
                            fini = true;
                        }

                        //Même chose que précédemment mais pour la ligne de la dernière pose
                        else if (mapDeJeu.GetMapTab()[x, j].IsFields() && (mapDeJeu.GetMapTab()[x, j].GetCestChezQui() == "personne"))
                        {
                            coordsec = x.ToString() + j.ToString();
                            fini = true;
                        }
                    }

                }
            }
            return coordsec; //On retourne les coordonnées via une chaîne de caractère
        }

        //Procédure du cerveau de l'IA
        public void Cerveau()
        {
            bool aJouer = false;
            bool fini = false;
            bool premierCoup = true;
            bool premierCoupJouer = false;
            string poseServeur = null;
            string messageServeur = null;
            int xServeur = 0;
            int yServeur = 0;

            //On joue tant que le serveur n'envoie la pas la chaîne de caractère "FINI"
            while (!fini)
            {
                //Si le premier coup n'a pas été joué, on le joue
                if (premierCoup)
                {
                    //On parcourt toute la carte car la première pose est libre
                    for (int i = 0; i < 10; i++)
                    {
                        for (int j = 0; j < 10; j++)
                        {
                            //On cherche une parcelle de 2
                            if (mapDeJeu.GetMapTab()[i, j].GetTailleParcelle() == 2 && !premierCoupJouer)
                            {
                                CommServeur.Envoi("A:" + i + j); //Si une parcelle de 2, on envoie les coordonnées au serveur
                                mapDeJeu.GetMapTab()[i, j].SetCestChezQui("IA"); //On set l'appartenance de la parcelle à l'IA
                                premierCoupJouer = true; //On passe le booléen du premier coup joué à true pour ne pas le rejouer tout le temps
                                CommServeur.Reception(4); //On réceptionne ce que le serveur nous envoie "VALI" ou "INVA"

                            }
                        }

                    }
                    premierCoup = false;

                }

                //On réceptionne les 2 chaîne de caractère envoyer par le serveur
                aJouer = false;
                poseServeur = CommServeur.Reception(4);
                messageServeur = CommServeur.Reception(4);

                //Si le serveur nous envoie "ENCO" c'est qu'on peut encore jouer
                if (messageServeur == "ENCO")
                {
                    //On récupère les coordonnées de la dernière pose grâce à substring
                    xServeur = int.Parse(poseServeur.Substring(2, 1));
                    yServeur = int.Parse(poseServeur.Substring(3, 1));
                    mapDeJeu.GetMapTab()[xServeur, yServeur].SetCestChezQui("Serveur"); //On set l'appartenance de la case au Serveur

                    //Si dans notre liste de pose possible, il y a une case d'une parcelle de 2 alors on pose
                    if (PosesPossible(xServeur, yServeur, 2).Count() > 0)
                    {
                        string poseCoupA2 = PosesPossible(xServeur, yServeur, 2).ElementAt(0);
                        CommServeur.Envoi("A:" + poseCoupA2); //Envoie des coordonnées de notre pose via chaîne de caractère

                        if (CommServeur.Reception(4) == "VALI")
                        {
                            //Si le serveur renvoie "VALI", on set l'appartenance de la case à l'IA
                            mapDeJeu.GetMapTab()[int.Parse(poseCoupA2.Substring(0, 1)), int.Parse(poseCoupA2.Substring(1, 1))].SetCestChezQui("IA");
                        }
                        aJouer = true;
                    }

                    //Sinon si le serveur ne peut pas jouer sur une parcelle de 2, on refait la même chose pour une parcelle de 3
                    else if(PosesPossible(xServeur, yServeur, 3).Count() > 0)
                    {
                        //On parcourt la liste de nos poses potentielles
                        foreach (string item in PosesPossible(xServeur, yServeur,3))
                        {
                            //Si on a pas encore jouer 
                            if (!aJouer)
                            {
                                //Si les condition de la fonction "OnPeuxJouerOuPas" sont remplis on envoie au serveur les coordonnées récupéré dans la liste des poses possibles
                                if (OnPeuxJouerOuPas(item, 2))
                                {
                                    CommServeur.Envoi("A:" + item);
                                    //Si le serveur r'envoie "VALI" après notre envoi, on passe l'appartenance de cette case l'IA
                                    if(CommServeur.Reception(4) == "VALI")
                                    {
                                        mapDeJeu.GetMapTab()[int.Parse(item.Substring(0, 1)), int.Parse(item.Substring(1, 1))].SetCestChezQui("IA");
                                    }
                                    aJouer = true; //Après avoir jouer on passe le booléen "aJouer" à true pour ne pas jouer plusieurs fois de suite
                                    
                                }
                            }
                           
                        }

                    //Même chose que précédement mais pour une parcelle de 4
                    }else if(PosesPossible(xServeur, yServeur, 4).Count() > 0)
                    {
                        foreach (string item in PosesPossible(xServeur, yServeur, 4))
                        {
                            if (!aJouer)
                            {
                                if (OnPeuxJouerOuPas(item, 3))
                                {
                                    CommServeur.Envoi("A:" + item);
                                    if (CommServeur.Reception(4) == "VALI")
                                    {
                                        mapDeJeu.GetMapTab()[int.Parse(item.Substring(0, 1)), int.Parse(item.Substring(1, 1))].SetCestChezQui("IA");
                                    }
                                    aJouer = true;
                                }
                            }

                        }
                    //Même chose que précédemment mais pour une parcelle de 6
                    }else if(PosesPossible(xServeur, yServeur, 6).Count() > 0)
                    {
                        foreach (string item in PosesPossible(xServeur, yServeur, 6))
                        {
                            if (!aJouer)
                            {
                                if (OnPeuxJouerOuPas(item, 4))
                                {
                                    CommServeur.Envoi("A:" + item);
                                    if (CommServeur.Reception(4) == "VALI")
                                    {
                                        mapDeJeu.GetMapTab()[int.Parse(item.Substring(0, 1)), int.Parse(item.Substring(1, 1))].SetCestChezQui("IA");
                                    }
                                    aJouer = true;
                                }
                            }

                        }
                    }
                    //Sinon si notre stratégie ne permet pas de poser sur une parcelle de 2, 3, 4 ou 6, on utilise la méthode de sécurisation afin de ne pas gaspiller un coup 
                    else
                    {
                        string poseSecu = StratSecours(xServeur, yServeur);
                        CommServeur.Envoi("A:" + poseSecu);
                        if (CommServeur.Reception(4) == "VALI")
                        {
                            mapDeJeu.GetMapTab()[int.Parse(poseSecu.Substring(0, 1)), int.Parse(poseSecu.Substring(1, 1))].SetCestChezQui("IA");
                        }
                    }
                    
                }
                //Si le serveur envoie le message "FINI", alors le jeu est terminé
                if (messageServeur == "FINI")
                {
                    fini = true; //On passe le booléen fini à true

                }

            }
        }

    }
}
