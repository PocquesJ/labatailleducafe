﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LaBatailleDuCafe
{
    //Classe instanciant les cases de la carte
    public class Case
    {
        //Déclaration des variables
        private char identifier; //Caractère définissant un identifiant des cases comprises sur la même parcelle
        private int value; //Entier représentant la valeur de la case, nécessaire pour savoir de quel type est la case
        private bool sea;//Booléen qui représentera une case de type "Mer"
        private bool forest;//Booléen qui représentera une case de type "Forest"
        private bool fields;//Booléen qui représentera une case de type "Terre"
        private bool frontierSouth;//Booléens représentant si une case à une/des frontière(s), nécessaire au décodage de la carte
        private bool frontierNorth;
        private bool frontierEast;
        private bool frontierWest;
        private string CestChezQui;//Chaîne de caractère indiquant à qui appartient la case
        private int tailleParcelle;//Entier représentant la taille d'une parcelle

        //Constructeur des cases, passage de tout les booléens à false par défaut
        public Case(int value)
        {
            this.value = value;
            this.sea = false;
            this.forest = false;
            this.fields = false;
            this.frontierSouth = false;
            this.frontierNorth = false;
            this.frontierEast = false;
            this.frontierWest = false;
            this.identifier = ' ';
            this.CestChezQui = "personne";
            this.tailleParcelle = 0;
        }

        //Setter et getter de toutes les variables de la classe
        public int GetTailleParcelle()
        {
            return tailleParcelle;
        }
        public void SetTailleParcelle(int tailleParcelle)
        {
            this.tailleParcelle = tailleParcelle;
        }
        public string GetCestChezQui()
        {
            return CestChezQui;
        }
        public void SetCestChezQui(string CestChezQui)
        {
            this.CestChezQui = CestChezQui;
        }
        public char GetIdentifer()
        {
            return identifier;
        }
        public void SetIdentifier(char identifier)
        {
            this.identifier = identifier;
        }
        public int GetValue()
        {
            return value;
        }
        public void SetValue(int value)
        {
            this.value = value;
        }
        public bool IsSea()
        {
            return sea;
        }
        public void SetSea(bool sea)
        {
            this.sea = sea;
        }
        public bool IsForest()
        {
            return forest;
        }
        public void SetForest(bool forest)
        {
            this.forest = forest;
        }
        public bool IsFields()
        {
            return fields;
        }
        public void SetFields(bool fields)
        {
            this.fields = fields;
        }
        public bool HasFrontierSouth()
        {
            return frontierSouth;
        }
        public void SetFrontierSouth(bool frontierSouth)
        {
            this.frontierSouth = frontierSouth;
        }
        public bool HasFrontierNorth()
        {
            return frontierNorth;
        }
        public void SetFrontierNorth(bool frontierNorth)
        {
            this.frontierNorth = frontierNorth;
        }
        public bool HasFrontierEast()
        {
            return frontierEast;
        }
        public void SetFrontierEast(bool frontierEast)
        {
            this.frontierEast = frontierEast;
        }
        public bool HasFrontierWest()
        {
            return frontierWest;
        }
        public void SetFrontierWest(bool frontierWest)
        {
            this.frontierWest = frontierWest;
        }

        //Procédure plaçant les cases de type "Mer" ou de type "Forêt"
        public void WhatNature()
        {
            if (this.GetValue() >= 64) //Vérif si il s'agit d'une case de type "mer" grâce à sa valeur
            {
                this.SetSea(true); //Passage du booléen de "Mer" à true 
                this.SetValue(this.GetValue() - 64); //On replace la variable "value" à null
                this.SetIdentifier('M'); //On set l'identifiant de la case à "M" pour "Mer"
            }
            else if (this.GetValue() >= 32 && this.GetValue() <= 64) //Vérif si il s'agit d'une case de type "forêt" grâce à sa valeur 
            {
                this.SetForest(true); //Passage du booléen de "Forêt" à true
                this.SetValue(this.GetValue() - 32); //On replace la variable "value" à null
                this.SetIdentifier('F'); //On set l'identifiant de la case à "F" pour "Forêt"

            }
            else //Sinon si la case n'est ni de type "Mer" ou de type "Forêt", il s'agit de "terre"
            {
                this.SetFields(true);
            }
        }

        //Procédure placant les frontières des cases 
        public void WhatFrontier()
        {
            if (this.GetValue() >= 8) //Vérifie si la case à une frontière à l'est
            {
                this.SetValue(this.GetValue() - 8); //Réinitialisation de la variable value à 0
                this.SetFrontierEast(true); //Passage du booléen de la frontière est à true
            }
            if (this.GetValue() >= 4) //Vérifie si la case à une frontière au sud
            {
                this.SetValue(this.GetValue() - 4); //Réinitialisation de la variable value à 0
                this.SetFrontierSouth(true); //Passage du booléen de la frontière sud à true
            }
            if (this.GetValue() >= 2) //Vérifie si la case à une frontière à l'ouest
            {
                this.SetValue(this.GetValue() - 2); //Réinitialisation de la variable value à 0
                this.SetFrontierWest(true); //Passage du booléen de la frontière ouest à true
            }
            if (this.GetValue() >= 1) //Vérifie si la case à une frontière au nord 
            {
                this.SetValue(this.GetValue() - 1); //Réinitialisation de la variable value à 0
                this.SetFrontierNorth(true); //Passage du booléen de la frontière nord à true
            }
        }
    }
}
