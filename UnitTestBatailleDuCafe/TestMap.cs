﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LaBatailleDuCafe;
namespace UnitTestBatailleDuCafe
{
    [TestClass]
    public class TestMap
    {
        [TestMethod]
        public void TestStorageMap()
        {
            Map mapTest = new Map();
            CommServeur SocketComm = new CommServeur("localhost", 1213);

            Assert.AreEqual(0, mapTest.GetMap().Count);
            mapTest.StorageMap(RecuperationTrame.Recuperation(SocketComm,"localhost", 1213).TrimEnd('|'));
            Assert.AreEqual(100, mapTest.GetMap().Count);
        }
    }
}
