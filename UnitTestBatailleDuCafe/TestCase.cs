﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LaBatailleDuCafe;
namespace UnitTestBatailleDuCafe
{
    [TestClass]
    public class TestCase
    {
        

        [TestMethod]
        public void TestConstructorCase()
        {
            Case caseTest = new Case(65);
            Assert.AreEqual(65, caseTest.GetValue());
            Assert.AreEqual(false, caseTest.IsFields());
            Assert.AreEqual(false, caseTest.IsSea());
            Assert.AreEqual(false, caseTest.IsForest());
            Assert.AreEqual(false, caseTest.HasFrontierEast());
            Assert.AreEqual(false, caseTest.HasFrontierNorth());
            Assert.AreEqual(false, caseTest.HasFrontierSouth());
            Assert.AreEqual(false,caseTest.HasFrontierWest());
        }

        [TestMethod]

        public void TestWhatNature()
        {
            Case caseTest = new Case(65);
            caseTest.WhatNature();

            Assert.AreEqual(true, caseTest.IsSea());
            Assert.AreEqual(false, caseTest.IsFields());
            Assert.AreEqual(false, caseTest.IsForest());
            Assert.AreEqual(1, caseTest.GetValue());

        }
        [TestMethod]

        public void TestWhatFrontier()
        {
            Case caseTest = new Case(65);
            caseTest.WhatNature();
            caseTest.WhatFrontier();
            Assert.AreEqual(true, caseTest.HasFrontierNorth());
            Assert.AreEqual(false, caseTest.HasFrontierEast());
            Assert.AreEqual(false, caseTest.HasFrontierSouth());
            Assert.AreEqual(false, caseTest.HasFrontierWest());
            Assert.AreEqual(0, caseTest.GetValue());
        }
        
    }
}
